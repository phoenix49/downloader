#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       downloadit.pyw
#       
#       Copyright 2012 Said Babayev <phoenix49@gmail.com>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       

import sys
import os.path
from PySide.QtCore import *
from PySide.QtGui import *
from lib import engine
from lib import classes
from lib import resources
from lib import ui_main

class Main(QMainWindow, ui_main.Ui_mainWindow):
    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        self.setupUi(self)
        #Initialize
        self.appinfo = classes.AppInfo()
        self.setWindowIcon(QIcon(":/download"))
        self.setWindowTitle(self.appinfo.appname + ' - ' + self.appinfo.appversion)
        self.newd = None #var for new download window
        self.prefs = {} #dict for preferences
        self.quit_ = False #var for quit from menu
        self.clipboard = QClipboard()
        self.stats = classes.Statistics(self)
        self.osFeat() #Initialize OS specific features
        self.tbTimer = QTimer() #Timer for taskbar update
        self.tbTimer.setInterval(1000)
        self.statuswidget = classes.StatusWidget()
        #self.statusbar.addWidget(self.statuswidget)
        self.setStatusBar(self.statuswidget)
        
        #Settings
        self.tableWidget.setColumnWidth(0, 300) #Filename
        self.tableWidget.setColumnWidth(1, 100) #Size
        self.tableWidget.setColumnWidth(2, 100) #Speed
        self.tableWidget.setColumnWidth(3, 200) #Progress
        self.tableWidget.setColumnWidth(4, 100) #Remaining
        self.tableWidget.setColumnWidth(5, 100) #Time to finish
        
        self.defaultddir = os.path.normpath(os.path.expanduser('~/Downloads'))
        self.readSettings()
        self.updateProxy()
        self.viewdic = {self.actionSize : 1,
                        self.actionSpeed : 2,
                        self.actionProgress : 3,
                        self.actionRemaining : 4,
                        self.actionTime_to_finish : 5}
        
        geometry = self.appinfo.settings.value("MainWindow/geometry")
        if geometry:
            self.restoreGeometry(geometry)
        self.tableWidget.verticalHeader().setResizeMode(QHeaderView.Fixed)
        
        #set icons and fallback icons for other platforms
        self.actionNew.setIcon(QIcon(":/newDownload"))
        self.actionStart_queue.setIcon(QIcon(":/startQueue"))
        self.actionStop_queue.setIcon(QIcon(":/stopQueue"))
        self.actionClear_completed.setIcon(QIcon(":/downloadFinished"))
        self.actionStartDownload.setIcon(QIcon(":/startDownload"))
        self.actionPauseDownload.setIcon(QIcon(":/pauseDownload"))
        self.actionCancelDownload.setIcon(QIcon(":/cancelDownload"))
        self.actionMove_up.setIcon(QIcon(":/moveUp"))
        self.actionMove_down.setIcon(QIcon(":/moveDown"))
        self.actionLow.setIcon(QIcon(":/low"))
        self.actionMedium.setIcon(QIcon(":/medium"))
        self.actionFull.setIcon(QIcon(":/full"))
        
        #Initialize engine
        self.core = engine.ControlCore(ddir=self.prefs['ddir'], qmgr=self.prefs['queue'], proxy=self.proxy)

        #Read data from file and populate the list
        if self.core.grabbers != []:
            for i in range(len(self.core.grabbers)):
                self.updateView(i)
                self.updateIcon(i)
                self.core.grabbers[i][1].datasig.connect(self.updateView)
                self.core.grabbers[i][1].errsig.connect(self.addError)
                self.core.grabbers[i][1].finsig.connect(self.dowFinished)
                self.core.grabbers[i][0].errmsgsig.connect(self.grabberError)
        
        #Run at startup
        if self.prefs['resume']:
            self.startQueue()
        
        #Buttons & Actions
        self.updateMain(True)
        self.updateViewMenu()
        self.updateColumn()
        
        self.speedgroup = QActionGroup(self)
        self.speedgroup.addAction(self.actionLow)
        self.speedgroup.addAction(self.actionMedium)
        self.speedgroup.addAction(self.actionFull)
        
        self.actiongroup = QActionGroup(self)
        self.actiongroup.addAction(self.actionStartDownload)
        self.actiongroup.addAction(self.actionPauseDownload)
        
        #Signals and slots
        self.core.finishedsig.connect(self.finished)
        self.core.queuesig.connect(self.updateIcon)
        
        self.actionNew.triggered.connect(self.downloadWin)
        self.actionQuit.triggered.connect(self.quitProgram)
        self.actionStart_queue.triggered.connect(self.startQueue)
        self.actionStop_queue.triggered.connect(self.stopQueue)
        self.actionClear_completed.triggered.connect(self.clearCompleted)
        self.actionStartDownload.triggered.connect(self.startDownload)
        self.actionPauseDownload.triggered.connect(self.stopDownload)
        self.actionCancelDownload.triggered.connect(self.cancelDownload)
        self.actionRestartDownload.triggered.connect(self.restartDownload)
        self.actionMove_up.triggered.connect(self.moveUp)
        self.actionMove_down.triggered.connect(self.moveDown)
        self.actionLow.triggered.connect(self.speedLow)
        self.actionMedium.triggered.connect(self.speedMedium)
        self.actionFull.triggered.connect(self.speedFull)
        self.actionLaunch.triggered.connect(self.launchSignal)
        self.actionOpenFolder.triggered.connect(self.openFolder)
        self.actionStatistics.triggered.connect(self.showStats)
        self.actionPreferences.triggered.connect(self.showPrefs)
        self.actionAbout.triggered.connect(self.showAbout)
        
        for key in self.viewdic.iterkeys():
            key.triggered.connect(self.updateColumn)

        self.tableWidget.itemSelectionChanged.connect(self.updateMain)
        self.tbTimer.timeout.connect(self.tbUpdate)
        
        #Tablewidget context menu
        twOpenFolder = QAction(self.tr('Open containing folder'), self)
        twLaunchFile = QAction(self.tr('Launch file'), self)
        twProperties = QAction(self.tr('Properties'), self)
        twOpenFolder.triggered.connect(self.openFolder)
        twLaunchFile.triggered.connect(self.launchSignal)
        twProperties.triggered.connect(self.showProperties)
        self.tableWidget.addAction(twOpenFolder)
        self.tableWidget.addAction(twLaunchFile)
        self.tableWidget.addAction(twProperties)
        
    def readSettings(self):
        #Read settings, first apply default values
        self.prefs['ddir'] = str(self.appinfo.settings.value('prefs/ddir', self.defaultddir))
        self.prefs['queue'] = int(self.appinfo.settings.value('prefs/queue', 2))
        self.prefs['speedsetting'] = int(self.appinfo.settings.value('prefs/speedsetting', 2))
        self.prefs['speedlow'] = int(self.appinfo.settings.value('prefs/speedlow', 50000))
        self.prefs['speedmedium'] = int(self.appinfo.settings.value('prefs/speedmedium', 100000))
        self.prefs['deleteonerror'] = int(self.appinfo.settings.value('prefs/deleteonerror', 2))
        self.prefs['completed'] = int(self.appinfo.settings.value('prefs/completed', 0))
        self.prefs['start'] = int(self.appinfo.settings.value('prefs/start', 2))
        self.prefs['resume'] = int(self.appinfo.settings.value('prefs/resume', 0))
        self.prefs['close'] = int(self.appinfo.settings.value('prefs/close', 2))
        self.prefs['launch'] = int(self.appinfo.settings.value('prefs/launch', 0))
        self.prefs['clipboard'] = int(self.appinfo.settings.value('prefs/clipboard', 0))
        self.prefs['confirm'] = int(self.appinfo.settings.value('prefs/confirm', 2))
        self.prefs['proxytype'] = int(self.appinfo.settings.value('prefs/proxytype', 0))
        self.prefs['proxyaddress'] = str(self.appinfo.settings.value('prefs/proxyaddress', ''))
        self.prefs['proxyport'] = str(self.appinfo.settings.value('prefs/proxyport', '80'))
        self.prefs['proxyauth'] = int(self.appinfo.settings.value('prefs/proxyauth', 0))
        self.prefs['proxyusername'] = str(self.appinfo.settings.value('prefs/proxyusername', ''))
        self.prefs['proxypassword'] = str(self.appinfo.settings.value('prefs/proxypassword', ''))
        self.prefs['deleteincomplete'] = int(self.appinfo.settings.value('prefs/deleteincomplete', 2))
    
    def updateMain(self, state=None):
        if state == None:
            state = False
        self.actionStartDownload.setDisabled(state)
        self.actionPauseDownload.setDisabled(state)
        self.actionCancelDownload.setDisabled(state)
        self.actionLow.setDisabled(state)
        self.actionMedium.setDisabled(state)
        self.actionFull.setDisabled(state)
        self.actionMove_up.setDisabled(state)
        self.actionMove_down.setDisabled(state)
        self.actionRestartDownload.setDisabled(state)
        self.actionLaunch.setDisabled(state)
        self.actionOpenFolder.setDisabled(state)
        self.actionProperties.setDisabled(state)
        try:
            i = self.core.grabbers[self.tableWidget.currentRow()][1].info['throttleb']
            #i for speedlevel
            if self.tableWidget.currentRow() == -1:
                return
            if i == 0:
                self.actionLow.setChecked(True)
            elif i == 1:
                self.actionMedium.setChecked(True)
            elif i == 2:
                self.actionFull.setChecked(True)
            else:
                self.actionFull.setChecked(True)
            #Start/pause button
            if self.core.grabbers[self.tableWidget.currentRow()][0].running or \
                    self.core.grabbers[self.tableWidget.currentRow()][1].waiting is True:
                self.actionStartDownload.setChecked(True)
                self.actionCancelDownload.setDisabled(True)
            else:
                self.actionPauseDownload.setChecked(True)
                self.actionCancelDownload.setDisabled(False)
            #Statusbar update
            row = self.tableWidget.currentRow()
            self.updateStatusBar(row)
        except:
            pass
    
    def updateStatusBar(self, row):
        if row == self.tableWidget.currentRow():
            self.statuswidget.updateView(self.core.grabbers[row][1].info['url'], self.core.grabbers[row][1].getProgress())

    def updateViewMenu(self):
        self.actionSize.setChecked(bool(self.appinfo.settings.value('view/size', 1)))
        self.actionSpeed.setChecked(bool(self.appinfo.settings.value('view/progress', 1)))
        self.actionProgress.setChecked(bool(self.appinfo.settings.value('view/downloaded', 1)))
        self.actionRemaining.setChecked(bool(self.appinfo.settings.value('view/remaining', 1)))
        self.actionTime_to_finish.setChecked(bool(self.appinfo.settings.value('view/ttf', 1)))
    
    def saveViewMenu(self):
        self.appinfo.settings.setValue('view/size', self.convertBool(self.actionSize.isChecked()))
        self.appinfo.settings.setValue('view/progress', self.convertBool(self.actionSpeed.isChecked()))
        self.appinfo.settings.setValue('view/downloaded', self.convertBool(self.actionProgress.isChecked()))
        self.appinfo.settings.setValue('view/remaining', self.convertBool(self.actionRemaining.isChecked()))
        self.appinfo.settings.setValue('view/ttf', self.convertBool(self.actionTime_to_finish.isChecked()))
    
    def updateColumn(self):
        for action, value in self.viewdic.items():
            if action.isChecked():
                self.tableWidget.showColumn(value)
            else:
                self.tableWidget.hideColumn(value)
    
    def convertBool(self, boolvalue):
        if boolvalue:
            return 1
        else:
            return 0
    
    def updateView(self, i):
        #Update table depending on row
        if not self.tableWidget.rowCount() > i:
            self.tableWidget.insertRow(i)
            name = self.core.grabbers[i][1].getFname()
            if len(name) == 0:
                name = self.core.grabbers[i][1].info['url']
            fname = QTableWidgetItem(name)
            size = QTableWidgetItem(self.core.grabbers[i][1].getSize())
            speed = QTableWidgetItem(self.core.grabbers[i][1].getSpeed())
            remaining = QTableWidgetItem(self.core.grabbers[i][1].getRemaining())
            ttf = QTableWidgetItem(self.core.grabbers[i][1].getTTF())
            self.tableWidget.setItem(i, 0, fname)
            self.tableWidget.setItem(i, 1, size)
            self.tableWidget.setItem(i, 2, speed)
            pbw = classes.ProgressBar(self.core.grabbers[i][1].info['progress'])
            self.tableWidget.setCellWidget(i, 3, pbw)
            self.tableWidget.setItem(i, 4, remaining)
            self.tableWidget.setItem(i, 5, ttf)
        else:
            name = self.core.grabbers[i][1].getFname()
            if len(name) == 0:
                name = self.core.grabbers[i][1].info['url']
            self.tableWidget.item(i, 0).setText(name)
            self.tableWidget.item(i, 1).setText(self.core.grabbers[i][1].getSize())
            self.tableWidget.item(i, 2).setText(self.core.grabbers[i][1].getSpeed())
            self.stats.updateSpeed(self.core.grabbers[i][1].info['speed'])
            self.tableWidget.cellWidget(i, 3).setValue(self.core.grabbers[i][1].info['progress'])
            self.stats.updateDownloaded(self.core.grabbers[i][1].info['delta'])
            self.tableWidget.item(i, 4).setText(self.core.grabbers[i][1].getRemaining())
            self.tableWidget.item(i, 5).setText(self.core.grabbers[i][1].getTTF())
        if self.core.grabbers[i][1].info['progress'] == 100:
            self.updateMain()
        self.updateStatusBar(i)
    
    def updateIcon(self, i):
        #Update job icon
        if self.core.grabbers[i][0].running:
            self.tableWidget.item(i, 0).setIcon(QIcon(":/play"))
        elif self.core.grabbers[i][1].info['progress'] == 100:
            self.tableWidget.item(i, 0).setIcon(QIcon(":/check"))
        elif self.core.grabbers[i][1].waiting:
            self.tableWidget.item(i, 0).setIcon(QIcon(":/time"))
        else:
            self.tableWidget.item(i, 0).setIcon(QIcon(":/pause"))
    
    def dowFinished(self, i):
        #Special signal when download finishes
        self.core.grabbers[i][1].info['speed'] = 0
        self.core.grabbers[i][1].info['ttf'] = 0
        self.updateView(i)
        self.updateIcon(i)
        if self.prefs['launch'] and not self.core.grabbers[i][1].info['launched']:
            self.launchFile(i)
        
    def launchFile(self, i):
        self.core.grabbers[i][1].info['launched'] = True
        url = QUrl.fromLocalFile(self.core.grabbers[i][1].info['path'] + self.core.grabbers[i][1].info['fname'])
        QDesktopServices.openUrl(url)
    
    def launchSignal(self):
        i = self.tableWidget.currentRow()
        if self.core.grabbers[i][1].info['progress'] == 100:
            self.launchFile(i)
        else:
            QMessageBox.information(self, self.tr('Launch file'), \
                        self.tr('Job is incomplete'), QMessageBox.Ok)
    
    def finished(self, i):
        self.updateView(i)
        self.updateIcon(i)
        self.updateMain()
    
    def downloadWin(self):
        #Show new download window
        self.newd = classes.DownloadWin(self.tr('New Download'), self.prefs['ddir'], \
                        self.prefs['speedsetting'], parent=self)
        self.newd.accepted.connect(self.addDownload)
        self.newd.rejected.connect(self.clearNewd)
        if self.prefs['clipboard']:
            clip = self.analyzeClipboard()
            if clip is not None:
                self.newd.urlEdit.setText(clip)
        self.newd.show()
    
    def showProperties(self):
        i = self.tableWidget.currentRow()
        self.props = classes.DownloadWin(self.tr('Download Properties'), self.core.grabbers[i][1].info['path'], \
                        self.core.grabbers[i][1].info['throttleb'], parent=self)
        self.props.urlEdit.setText(self.core.grabbers[i][1].info['url'])
        self.props.urlEdit.setCursorPosition(0)
        self.props.urlEdit.setDisabled(True)
        if self.core.grabbers[i][0].running:
            self.props.pathEdit.setDisabled(True)
        self.props.accepted.connect(self.propsUpdated)
        self.props.show()
    
    def propsUpdated(self):
        #Properties window accepted
        i = self.tableWidget.currentRow()
        origpath = self.core.grabbers[i][1].info['path']
        newpath = self.props.pathEdit.text()
        #If path changed but speed is not
        if not origpath == newpath:
            self.core.changePath(i, origpath, newpath)
        self.speedDecision(self.props.speed)
    
    def speedDecision(self, speed):
        if speed == 0:
            self.speedLow()
        elif speed == 1:
            self.speedMedium()
        else:
            self.speedFull()
        self.updateMain()
    
    def analyzeClipboard(self):
        text = self.clipboard.text()
        if text == '':
            return None
        ind = text.find('://')
        if ind > 0:
            return text
        else:
            return None
    
    def clearNewd(self):
        self.newd.urlEdit.clear()
    
    def quitProgram(self):
        self.quit_ = True
        self.close()
        self.quit_ = False
    
    def addDownload(self):
        #Add new download with provided parameters and update table
        url = str(self.newd.urlEdit.text())
        if not len(url) > 0:
            return
        path = self.newd.pathEdit.text()
        i = len(self.core.grabbers)
        self.core.addGrabber(url, path)
        self.newd.urlEdit.clear()
        self.updateView(i)
        self.updateIcon(i)
        self.core.grabbers[i][1].datasig.connect(self.updateView)
        self.core.grabbers[i][1].errsig.connect(self.addError)
        self.core.grabbers[i][1].finsig.connect(self.dowFinished)
        self.core.grabbers[i][0].errmsgsig.connect(self.grabberError)
        if self.newd.lowSpeed.isChecked():
            self.core.setSpeed(i, self.prefs['speedlow'], 0)
        elif self.newd.mediumSpeed.isChecked():
            self.core.setSpeed(i, self.prefs['speedmedium'], 1)
        else:
            self.core.setSpeed(i, 0, 2)
        #Start download instantly
        if self.prefs['start']:
            self.startDownload(i)
        self.stats.newJob()
    
    def addError(self, exception):
        QMessageBox.warning(self, self.tr('Error occured'), exception)
        if self.prefs['deleteonerror']:
            self.cancelDownload(row=self.tableWidget.rowCount()-1)
    
    def grabberError(self, exception):
        QMessageBox.warning(self, self.tr('HTTP Error'), exception)
        self.stopDownload()
    
    def startQueue(self):
        if self.tableWidget.rowCount() > 0: #Don't start if queue is empty; autostart
            for i in range(len(self.core.grabbers)):
                if not self.core.grabbers[i][0].running:
                    self.startDownload(i)
            self.updateView(self.tableWidget.currentRow())
            # Explicitly select current row
            self.tableWidget.setCurrentCell(self.tableWidget.currentRow(), 0)
    
    def stopQueue(self, q=False):
        if q:
            for i in range(len(self.core.grabbers)):
                self.core.quitJob(i)
        else:
            for i in range(len(self.core.grabbers)):
                self.core.stopGrabber(i)
        for i in range(len(self.core.grabbers)):
            self.updateView(i)
            self.updateIcon(i)
        self.core.queueUpdated()
        self.tableWidget.setCurrentCell(self.tableWidget.currentRow(), 0)
    
    def clearCompleted(self, ask=True):
        if ask is True:
            q = QMessageBox.question(self, self.tr('Clear Completed'), \
                self.tr('Are you sure you wish to clear completed jobs?'), QMessageBox.Ok | QMessageBox.Cancel)
            if q == QMessageBox.Ok:
                jobs = 0
                for i in range(len(self.core.grabbers)):
                    reali = i - jobs
                    if self.core.grabbers[reali][1].info['progress'] == 100:
                        self.cancelDownload(row=reali)
                        jobs += 1
                if jobs > 0:
                    QMessageBox.information(self, self.tr('Jobs Cleared'), \
                        self.tr('Jobs cleared: ') + str(jobs), QMessageBox.Ok)
                else:
                    QMessageBox.information(self, self.tr('Jobs Cleared'), \
                        self.tr('Completed jobs not found'), QMessageBox.Ok)
        else:
            jobs = 0
            for i in range(len(self.core.grabbers)):
                reali = i - jobs
                if self.core.grabbers[reali][1].info['progress'] == 100:
                    self.cancelDownload(row=reali)
                    jobs += 1
    
    def openFolder(self):
        i = self.tableWidget.currentRow()
        path = self.core.grabbers[i][1].info['path']
        url = QUrl.fromLocalFile(path)
        QDesktopServices.openUrl(url)

    def restartDownload(self):
        i = self.tableWidget.currentRow()
        q = QMessageBox.question(self, self.tr('Restart Download'), \
                self.tr('Are you sure you wish to redownload {0} ?'.format(self.core.grabbers[i][1].getFname())), QMessageBox.Ok | QMessageBox.Cancel)
        
    def startDownload(self, i=None):
        self.tbUpdate()
        self.tbTimer.start()
        if i is None:
            i = self.tableWidget.currentRow()
        self.core.resumeGrabber(i)
        self.updateIcon(i)
        self.actionCancelDownload.setDisabled(True)
    
    def stopDownload(self):
        if self.tableWidget.currentRow() >= 0:
            self.core.stopGrabber(self.tableWidget.currentRow())
            self.updateView(self.tableWidget.currentRow())
            self.updateIcon(self.tableWidget.currentRow())
            self.core.queueUpdated()
            self.actionCancelDownload.setDisabled(False)
    
    def cancelDownload(self, row=None):
        if row is None:
            ask = True
        else:
            ask = False
        if self.tableWidget.currentRow() >= 0 and ask:
            row = self.tableWidget.currentRow()
            q = QMessageBox.question(self, self.tr('Cancel Download'), \
                self.tr('Are you sure you wish to cancel download of {0} ?'.format(self.core.grabbers[row][1].getFname())), QMessageBox.Ok | QMessageBox.Cancel)
            if q == QMessageBox.Ok:
                if not self.core.grabbers[row][1].info['downloaded'] == self.core.grabbers[row][1].info['size'] and self.prefs['deleteincomplete'] == 2:
                    #Delete incomplete job if option is on
                    os.remove(self.core.grabbers[row][1].info['path'] + self.core.grabbers[row][1].info['fname'])
                self.tableWidget.removeRow(row)
                self.core.removeGrabber(row)
                if self.tableWidget.rowCount() == 0:
                    self.updateMain(True)
        else:
            self.tableWidget.removeRow(row)
            self.core.removeGrabber(row)
            if self.tableWidget.rowCount() == 0:
                self.updateMain(True)
    
    def moveUp(self):
        r = self.tableWidget.currentRow()
        if r == 0:
            return 0
        self.core.moveItem(r, r - 1)
        for i in range(len(self.core.grabbers)):
            self.updateView(i)
        self.tableWidget.setCurrentCell(r - 1, 0)
        
    def moveDown(self):
        r = self.tableWidget.currentRow()
        if r == self.tableWidget.rowCount() - 1 :
            return 0
        self.core.moveItem(r, r + 1)
        for i in range(len(self.core.grabbers)):
            self.updateView(i)
        self.tableWidget.setCurrentCell(r + 1, 0)

    def speedLow(self):
        self.speedgroup.setDisabled(True) #Race condition fix
        self.core.setSpeed(self.tableWidget.currentRow(), self.prefs['speedlow'], 0)
        self.speedgroup.setDisabled(False)
        
    def speedMedium(self):
        self.speedgroup.setDisabled(True)
        self.core.setSpeed(self.tableWidget.currentRow(), self.prefs['speedmedium'], 1)
        self.speedgroup.setDisabled(False)
        
    def speedFull(self):
        self.speedgroup.setDisabled(True)
        self.core.setSpeed(self.tableWidget.currentRow(), 0, 2)
        self.speedgroup.setDisabled(False)
        
    def showPrefs(self):
        self.pwin = classes.Preferences(self.prefs, self.appinfo.settings)
        self.pwin.accepted.connect(self.prefAccepted)
        self.pwin.show()
    
    def showAbout(self):
        self.dialog = classes.About(self)
        self.dialog.logoLabel.setPixmap(QPixmap(":/download"))
        self.dialog.setWindowTitle(self.tr('About ')+ self.appinfo.appname)
        self.dialog.titleLabel.setText(self.appinfo.appname)
        self.dialog.versionLabel.setText(self.tr('version ') + self.appinfo.appversion)
        self.dialog.aboutLabel.setText(self.tr(self.appinfo.description))
        self.dialog.rightsLabel.setText(self.tr('Copyright') + u' © 2011 ' + self.appinfo.devname)
        self.dialog.connect(self.dialog.closeButton, SIGNAL("clicked()"), self.dialog.close)
        self.dialog.show()
    
    def showStats(self):
        self.stats.showWin()

    def prefAccepted(self):
        self.readSettings()
        self.core.ddir = self.prefs['ddir']
        if not self.core.qmgr.totaltokens == self.prefs['queue']:
            self.core.qmgr.updateTokens(self.prefs['queue'])
            self.core.queueUpdated()
        self.updateProxy()
        if self.core.grabbers != []:
            for i in range(len(self.core.grabbers)):
                self.updateView(i)
    
    def updateProxy(self):
        proxyproto = 'http'
        proxy = None #System settings
        if self.prefs['proxytype'] == 1: #HTTP proxy
            if self.prefs['proxyauth']:
                authstr = self.prefs['proxyusername'] + ':' + self.prefs['proxypassword'] + '@'
            else:
                authstr = ''
            proxystr = proxyproto + '://' + authstr + self.prefs['proxyaddress'] + ':' + self.prefs['proxyport'] + '/'
            proxy = {proxyproto : proxystr}
        elif self.prefs['proxytype'] == 2: #Don't use proxy
            proxy = {}
        if hasattr(self, 'core'):
            self.core.setProxy(proxy)
        self.proxy = proxy
    
    def osFeat(self):
        #Construct interface to work with taskbars
        if sys.platform == 'win32':
            from winlib.win import WinTaskbar as Taskbar
            self.tb = Taskbar(self.winId())
        elif sys.platform.startswith('linux'):
            from ubulib.ubu import UbuUnity as Taskbar
            self.tb = Taskbar()
            self.tb.newDownloadQl.connect('item-activated', self.qlNewDownload) #Quicklist
            self.tb.startQueueQl.connect('item-activated', self.qlStartQueue)
            self.tb.stopQueueQl.connect('item-activated', self.qlStopQueue)
    
    def qlNewDownload(self, *args):
        self.downloadWin()
    
    def qlStartQueue(self, *args):
        self.startQueue()
    
    def qlStopQueue(self, *args):
        self.stopQueue()
    
    def tbUpdate(self):
        tbsum = 0
        tbcount = 0
        for pair in self.core.grabbers:
            if not pair[1].info['progress'] == 100 and pair[0].running:
                tbsum += pair[1].info['progress']
                tbcount += 1
        try:
            tbvalue = tbsum / tbcount
        except ZeroDivisionError:
            #In case if no jobs running - stop the update timer and update indicator to 0
            tbvalue = 0
            self.tbTimer.stop()
        self.tb.updateView(tbvalue, tbcount)
    
    def closeEvent(self, event):
        #Reimplement close event, event is a var and save window settings
        #Check if minimize instead of close
        if self.prefs['close'] and not self.quit_:
            self.showMinimized()
            event.ignore()
            return
        #Check if confirm is enabled in options
        if self.prefs['confirm']:
            ask = QMessageBox.question(self, self.tr('Quit'), \
                self.tr('Are you sure you wish to quit?'), QMessageBox.Ok | QMessageBox.Cancel)
            if ask == QMessageBox.Cancel:
                event.ignore()
                return
        self.appinfo.settings.setValue("MainWindow/geometry", self.saveGeometry())
        self.saveViewMenu()
        self.stopQueue()
        self.stats.saveTotal()
        if self.prefs['completed']:
            self.clearCompleted(ask=False)
        self.core.saveQueue()
        event.accept()

def main():
    #Disable output to console
    #sys.stdout = os.devnull
    #sys.stderr = os.devnull
    app = QApplication(sys.argv)
    #Findout syslocale
    locale = QLocale.system().name()
    #find and activate some system translations
    #qtTranslator = QTranslator()
    #if qtTranslator.load("qt_" + locale):
    #    app.installTranslator(qtTranslator)
    #custom translations
    #appTranslator = QTranslator()
    #if appTranslator.load(":/dit_" + locale):
    #    app.installTranslator(appTranslator)
    window = Main()
    window.show()
    app.exec_()
    return 0

if __name__ == '__main__':
    main()
