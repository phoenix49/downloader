#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       ubu.py
#       
#       Copyright 2012 Said Babayev <phoenix49@gmail.com>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       
#       
from gi.repository import Unity, Gio, Dbusmenu

class UbuUnity(object):
    def __init__(self, parent=None):
        super(UbuUnity, self).__init__()
        self.launcher = Unity.LauncherEntry.get_for_desktop_id ("downloader.desktop")
        
        #Quicklist
        self.ql = Dbusmenu.Menuitem.new()
        self.newDownloadQl = Dbusmenu.Menuitem.new()
        self.startQueueQl = Dbusmenu.Menuitem.new()
        self.stopQueueQl = Dbusmenu.Menuitem.new()
        
        self.newDownloadQl.property_set(Dbusmenu.MENUITEM_PROP_LABEL, "New download")
        self.startQueueQl.property_set(Dbusmenu.MENUITEM_PROP_LABEL, "Start queue")
        self.stopQueueQl.property_set(Dbusmenu.MENUITEM_PROP_LABEL, "Stop queue")
        
        self.ql.child_append(self.newDownloadQl)
        self.ql.child_append(self.startQueueQl)
        self.ql.child_append(self.stopQueueQl)
        
        self.launcher.set_property('quicklist', self.ql)
        
    def updateView(self, progress, count):
        self.launcher.set_property('progress_visible', True)
        self.launcher.set_property('progress', float(progress)/100)
        
        self.launcher.set_property('count_visible', True)
        self.launcher.set_property('count', count)
