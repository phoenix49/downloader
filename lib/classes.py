#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       classes.py
#       
#       Copyright 2012 Said Babayev <tesaid@azercell.com>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       
#       

import os
import os.path
import time
import sys
from PySide.QtCore import *
from PySide.QtGui import *
import ui_downloadwin
import ui_prefs
import ui_stats
import ui_about

class AppInfo:
    def __init__(self, parent=None):
        self.appname = 'Downloader'
        self.sysname = 'downloader'
        self.appversion = '0.8.30'
        self.settings = QSettings("Firesoft", "Downloader")
        self.devname = 'Said Babayev'
        self.devemail = 'phoenix49@gmail.com'
        self.url = 'https://bitbucket.org/phoenix49/downloader'
        self.description = 'Free download manager with minimalistic user interface'

class Calculator(object):
    #Calculator object for converting data into Human readable form
    def calculateSize(self, size):
        #Divide size by zeros to get Kbytes, Mbytes, etc
        if len(str(int(size))) > 9:
            size = str(int(size) / 1000000000) + ' GB'
        elif len(str(int(size))) > 6:
            size = str(int(size) / 1000000) + ' MB'
        elif len(str(int(size))) > 3:
            size = str(int(size) / 1000) + ' KB'
        else:
            size = str(int(size)) + ' B'
        return size
    
    def calculateTime(self, seconds):
        if seconds < 60:
            return str(seconds) + ' sec'
        elif seconds < 3600:
            gm = time.gmtime(seconds)
            m = time.strftime('%M', gm).lstrip('0')
            s = time.strftime('%S', gm).lstrip('0')
            return m + ' min ' + s + ' sec'
        elif seconds < 86400:
            gm = time.gmtime(seconds)
            h = time.strftime('%H', gm).lstrip('0')
            m = time.strftime('%M', gm).lstrip('0')
            return h + ' hr ' + m + ' min'
        else:
            gm = time.gmtime(seconds)
            d = time.strftime('%d', gm).lstrip('0')
            h = time.strftime('%H', gm).lstrip('0')
            return d + ' day ' + h + ' hr'

class Statistics(QDialog, ui_stats.Ui_StatDia, Calculator):
    def __init__(self, parent=None):
        super(Statistics, self).__init__(parent)
        self.setupUi(self)
        self.timer = QTimer()
        self.timer.setInterval(1000)
        self.total = {'downloaded':0,
                        'topspeed':0,
                        'jobs':0}
        self.current = self.total
        if sys.platform == 'win32':
            location = str(os.environ['appdata'] + '/Firesoft')
        else:
            location = '.config/Firesoft'
        self.wd = QDir(location)
        if not self.wd.exists():
            #Create dir if not exists
            self.wd.mkpath(location)
        self.sfile = QFile(self.wd.absolutePath() + '/stats.dit')
        if self.sfile.exists():
            self.sfile.open(QIODevice.ReadOnly)
            inp = QDataStream(self.sfile)
            #Check file format
            magno = inp.readInt32()
            if magno == 50:
                self.total = inp.readQVariant()
            self.sfile.close()
        
        #Signals
        self.resetButton.clicked.connect(self.resetData)
        self.timer.timeout.connect(self.updateView)
    
    def saveTotal(self):
        #Save totals to file
        self.sfile.open(QIODevice.WriteOnly)
        out = QDataStream(self.sfile)
        #Write header with magic no
        out.writeInt32(50)
        out.writeQVariant(self.total)
        self.sfile.close()
    
    def updateSpeed(self, speed):
        if speed > self.total['topspeed']:
            self.total['topspeed'] = speed
        if speed > self.current['topspeed']:
            self.current['topspeed'] = speed
    
    def updateDownloaded(self, downloaded):
        self.total['downloaded'] += downloaded
        self.current['downloaded'] += downloaded
    
    def newJob(self):
        self.total['jobs'] += 1
        self.current['jobs'] += 1
    
    def resetData(self):
        q = QMessageBox.question(self, self.tr('Reset statistic data'), \
                self.tr('Are you sure you wish to reset statistics?'), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            self.total['topspeed'] = 0
            self.total['downloaded'] = 0
            self.total['jobs'] = 0
            self.current['topspeed'] = 0
            self.current['downloaded'] = 0
            self.current['jobs'] = 0
        self.updateView()
    
    def showWin(self):
        self.updateView()
        self.timer.start()
        self.show()
    
    def closeEvent(self, event):
        self.timer.stop()
        event.accept()
    
    def updateView(self):
        self.topbytesLabel.setText(self.calculateSize(self.total['downloaded']))
        self.topspeedLabel.setText(self.calculateSize(self.total['topspeed']) + '/s')
        self.topjobsLabel.setText(str(self.total['jobs']))
        self.curbytesLabel.setText(self.calculateSize(self.current['downloaded']))
        self.curspeedLabel.setText(self.calculateSize(self.current['topspeed']) + '/s')
        self.curjobsLabel.setText(str(self.current['jobs']))

class ProgressBar(QWidget):
    #Progress bar widget
    def __init__(self, value, parent=None):
        super(ProgressBar, self).__init__(parent)
        #Put qprogressbar on widget and adjust layout
        self.pb = QProgressBar()
        self.pb.setMaximumHeight(15)
        self.setValue(value)
        layout = QVBoxLayout()
        layout.addWidget(self.pb)
        self.setLayout(layout)
    
    def setValue(self, val):
        self.pb.setValue(val)

class QueueManager(QObject):
    def __init__(self, tokens, parent=None):
        super(QueueManager, self).__init__(parent)
        self.totaltokens = tokens
        self.currenttokens = self.totaltokens
    
    def request(self):
        if self.currenttokens > 0:
            self.currenttokens -= 1
            return True
        else:
            return False
        
    def release(self):
        if self.currenttokens < self.totaltokens:
            self.currenttokens += 1
    
    def updateTokens(self, newtok):
        delta = newtok - self.totaltokens
        self.currenttokens += delta
        self.totaltokens = newtok

class Preferences(QDialog, ui_prefs.Ui_PrefDia):
    def __init__(self, prefs, settings, parent=None):
        #Preferences window class
        super(Preferences, self).__init__(parent)
        self.setupUi(self)
        self.prefs = prefs
        self.settings = settings
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setModal(True)
        
        #Initialize settings
        self.ddirLineEdit.setText(self.prefs['ddir'])
        self.queueSpinBox.setValue(int(self.prefs['queue']))
        self.speedComboBox.setCurrentIndex(int(self.prefs['speedsetting']))
        self.lowSpinBox.setValue(int(self.prefs['speedlow']) / 1000)
        self.mediumSpinBox.setValue(int(self.prefs['speedmedium']) / 1000)
        self.deleteCheckBox.setCheckState(Qt.CheckState(int(self.prefs['deleteonerror'])))
        self.completedCheckBox.setCheckState(Qt.CheckState(int(self.prefs['completed'])))
        self.startCheckBox.setCheckState(Qt.CheckState(int(self.prefs['start'])))
        self.resumeCheckBox.setCheckState(Qt.CheckState(int(self.prefs['resume'])))
        self.closeCheckBox.setCheckState(Qt.CheckState(int(self.prefs['close'])))
        self.launchCheckBox.setCheckState(Qt.CheckState(int(self.prefs['launch'])))
        self.clipboardCheckBox.setCheckState(Qt.CheckState(int(self.prefs['clipboard'])))
        self.confirmCheckBox.setCheckState(Qt.CheckState(int(self.prefs['confirm'])))
        self.ptypeComboBox.setCurrentIndex(int(self.prefs['proxytype']))
        self.paddressLineEdit.setText(self.prefs['proxyaddress'])
        self.pportLineEdit.setText(self.prefs['proxyport'])
        self.pauthCheckBox.setCheckState(Qt.CheckState(int(self.prefs['proxyauth'])))
        self.pusernameLineEdit.setText(self.prefs['proxyusername'])
        self.ppasswordLineEdit.setText(self.prefs['proxypassword'])
        self.deleteIncomplete.setCheckState(Qt.CheckState(int(self.prefs['deleteincomplete'])))
        
        self.lineValidator = FSValidator(self.ddirLineEdit.text())
        self.ptypeChanged()
        self.pauthChanged()
        
        #Signals & slots
        self.ddirToolButton.clicked.connect(self.selectFolder)
        self.ddirLineEdit.editingFinished.connect(self.checkFolder)
        self.ptypeComboBox.currentIndexChanged.connect(self.ptypeChanged)
        self.pauthCheckBox.stateChanged.connect(self.pauthChanged)
    
    def ptypeChanged(self):
        stateval = self.ptypeComboBox.currentIndex()
        if stateval == 1:
            state = True
        else:
            state = False
        self.paddressLineEdit.setEnabled(state)
        self.pportLineEdit.setEnabled(state)
        self.pauthCheckBox.setEnabled(state)
        self.pauthChanged()
    
    def pauthChanged(self):
        if self.pauthCheckBox.isEnabled():
            state = self.pauthCheckBox.checkState()
        else:
            state = False
        self.pusernameLineEdit.setEnabled(state)
        self.ppasswordLineEdit.setEnabled(state)
    
    def checkFolder(self):
        rightpath = self.lineValidator.validate(self.ddirLineEdit.text())
        self.ddirLineEdit.setText(os.path.normpath(rightpath))
    
    def selectFolder(self):
        folder = QFileDialog.getExistingDirectory(dir=self.ddirLineEdit.text())
        self.ddirLineEdit.setText(folder)
        self.lineValidator = FSValidator(self.ddirLineEdit.text())

    def accept(self):
        self.settings.setValue('prefs/ddir', self.ddirLineEdit.text())
        self.settings.setValue('prefs/queue', self.queueSpinBox.value())
        self.settings.setValue('prefs/speedsetting', self.speedComboBox.currentIndex())
        self.settings.setValue('prefs/speedlow', self.lowSpinBox.value() * 1000)
        self.settings.setValue('prefs/speedmedium', self.mediumSpinBox.value() * 1000)
        self.settings.setValue('prefs/deleteonerror', int(self.deleteCheckBox.checkState()))
        self.settings.setValue('prefs/completed', int(self.completedCheckBox.checkState()))
        self.settings.setValue('prefs/start', int(self.startCheckBox.checkState()))
        self.settings.setValue('prefs/resume', int(self.resumeCheckBox.checkState()))
        self.settings.setValue('prefs/close', int(self.closeCheckBox.checkState()))
        self.settings.setValue('prefs/launch', int(self.launchCheckBox.checkState()))
        self.settings.setValue('prefs/clipboard', int(self.clipboardCheckBox.checkState()))
        self.settings.setValue('prefs/confirm', int(self.confirmCheckBox.checkState()))
        self.settings.setValue('prefs/proxytype', self.ptypeComboBox.currentIndex())
        self.settings.setValue('prefs/proxyaddress', self.paddressLineEdit.text())
        self.settings.setValue('prefs/proxyport', self.pportLineEdit.text())
        self.settings.setValue('prefs/proxyauth', int(self.pauthCheckBox.checkState()))
        self.settings.setValue('prefs/proxyusername', self.pusernameLineEdit.text())
        self.settings.setValue('prefs/proxypassword', self.ppasswordLineEdit.text())
        self.settings.setValue('prefs/deleteincomplete', int(self.deleteIncomplete.checkState()))
        self.done(QDialog.Accepted)
    
    def reject(self):
        self.done(QDialog.Rejected)
        
class FSValidator:
    def __init__(self, path, parent=None):
        #Validator checks if path exists in filesystem
        self.path = path
    
    def validate(self, path):
        info = QFileInfo(path)
        if info.isDir():
            self.path = path
            return self.path
        else:
            return self.path

class DownloadWin(QDialog, ui_downloadwin.Ui_newDownload):
    def __init__(self, title, path, speed, parent=None):
        #New download dialog class
        super(DownloadWin, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(title)
        self.pathEdit.setText(path)
        self.lineValidator = FSValidator(path)
        self.speed = speed
        if speed == 0:
            self.lowSpeed.setChecked(True)
        elif speed == 1:
            self.mediumSpeed.setChecked(True)
        else:
            self.fullSpeed.setChecked(True)
        self.urlEdit.clear()
        self.browseButton.clicked.connect(self.selectFolder)
        self.pathEdit.editingFinished.connect(self.checkFolder)
        self.lowSpeed.clicked.connect(self.speedButton)
        self.mediumSpeed.clicked.connect(self.speedButton)
        self.fullSpeed.clicked.connect(self.speedButton)
    
    def speedButton(self):
		#Decide which speed button is pressed
		if self.lowSpeed.isChecked():
			self.speed = 0
		elif self.mediumSpeed.isChecked():
			self.speed = 1
		else:
			self.speed = 2
        
    def checkFolder(self):
        rightpath = self.lineValidator.validate(self.pathEdit.text())
        self.pathEdit.setText(os.path.normpath(rightpath))
    
    def selectFolder(self):
        folder = QFileDialog.getExistingDirectory(dir=self.pathEdit.text())
        self.pathEdit.setText(folder)
        self.lineValidator = FSValidator(self.pathEdit.text())

class About(QDialog, ui_about.Ui_aboutDialog):
    def __init__(self, parent=None):
        super(About, self).__init__(parent)
        self.setupUi(self)
        
class StatusWidget(QStatusBar):
    def __init__(self, parent=None):
        #status bar widget, currently unused
        super(StatusWidget, self).__init__(parent)
        self.url = ''
        self.progress = ''
        self.msgLabel = QLabel()
        self.addWidget(self.msgLabel)
    
    def updateView(self, url, progress):
        self.msgLabel.setText('{0} Progress: {1}'.format(url, progress))
