#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       engine.py
#       
#       Copyright 2012 Said Babayev <phoenix49@gmail.com>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       
#
import urllib2
import os
import sys
import shutil
from PySide.QtCore import *
import urlgrabber2
import urlgrabber2.progress
from classes import QueueManager
from classes import Calculator

class Grabber(QThread):
    #Class for single download
    errmsgsig = Signal(str)
    def __init__(self, url, downloaded, proxy=None, savelocation=None):
        super(Grabber, self).__init__()
        self.url = url
        self.savelocation = savelocation
        self.fname = ''
        self.progress = urlgrabber2.progress.BaseMeter(downloaded)
        self.reget = None
        self.throttle = 0
        self.proxy = proxy
        self.grabber = urlgrabber2.grabber.URLGrabber()
        self.running = False
    
    def runGrabber(self, command, throttle):
        #Pass a command
        self.throttle = throttle
        if command == 'start':
            self.reget = None
        elif command == 'resume':
            self.reget = 'check_timestamp'
        self.running = True
        self.start()
        
    def stopJob(self):
        #Wait until job stopped
        try:
            self.grabber.fo.terminate = True
            self.grabber.fo.stopsignal.connect(self.jobStopped)
        except:
            self.jobStopped()
    
    def jobStopped(self):
        #Job stopped
        try:
            self.grabber.fo.stopsignal.disconnect()
        except:
            pass
        self.running = False
    
    def run(self):
        if self.fname == '':
            self.fname = self.url[self.url.rfind('/'):]
            dto = self.savelocation + self.fname
        dto = self.savelocation + self.fname
        try:
            proxies = self.proxy
            try:
                self.grabber.fo.terminate = False
            except:
                pass
            self.grabber.urlgrab(self.url, filename = dto, progress_obj = self.progress, \
                                        reget = self.reget, throttle = self.throttle, \
                                        timeout = 30.0, useragent = 'dit', proxies=proxies)
        except:
            self.errmsgsig.emit(str(sys.exc_info()[1]))

class Metadata(QThread, Calculator):
    #Contains additional info on download
    datasig = Signal(int)
    errsig = Signal(str)
    finsig = Signal(int)
    def __init__(self, proxy=None, url=None, id=None):
        super(Metadata, self).__init__()
        self.info = {'url':url,
                    'fname':'',
                    'path':'',
                    'size':0,
                    'speed':0,
                    'progress':0,
                    'downloaded':0,
                    'delta':0,
                    'remaining':0,
                    'ttf':0,
                    'throttle':0,
                    'throttleb':2, #Speed setting
                    'proxy':proxy,
                    'launched':False}
        #id number in list
        self.id = id
        #Waiting flag for queue
        self.waiting = False
        self.finished = False

    def getFname(self):
        name = self.info['fname']
        if name.startswith('/'):
            return name[1:]
        else:
            return name
    
    def getSize(self):
        size = self.calculateSize(self.info['size'])
        return size
    
    def getSpeed(self):
        speed = self.calculateSize(self.info['speed']) + '/s'
        return speed
    
    def getProgress(self):
        prog = str(self.info['progress']) + '% / ' + self.calculateSize(self.info['downloaded'])
        return prog
    
    def getRemaining(self):
        self.info['remaining'] = self.info['size'] - self.info['downloaded']
        remaining = self.calculateSize(self.info['remaining'])
        return remaining
    
    def getTTF(self):
        ttf = self.calculateTime(self.info['ttf'])
        return ttf
    
    def speedSig(self, speed):
        if speed < 0:
            speed = 0
        self.info['speed'] = speed
        
    def readSig(self, data):
        #Also emit self.id to GUI update its view
        self.info['delta'] = data - self.info['downloaded']
        self.info['downloaded'] = data
        self.info['progress'] = self.info['downloaded'] * 100 / self.info['size']
        if not self.finished and self.info['progress'] == 100:
            #Emit separate signal when download finished
            self.finsig.emit(self.id)
            self.finished = True
        self.datasig.emit(self.id)
    
    def ttfSig(self, ttf):
        if ttf < 0:
            ttf = 0
        self.info['ttf'] = ttf
    
    def run(self):
        #update meta info for first time
        metaf = None
        try:
            proxy = urllib2.ProxyHandler(self.info['proxy'])
            opener = urllib2.build_opener(proxy)
            urllib2.install_opener(opener)
            metaf = urllib2.urlopen(self.info['url'])
        except urllib2.HTTPError as e:
            self.errsig.emit(str(e))
        except ValueError as e:
            self.errsig.emit('URL error')
        if metaf:
            metadic = dict(metaf.info())
            self.info['url'] = metaf.geturl()
            self.info['size'] = int(metadic['content-length'])
            self.info['fname'] = metaf.geturl()[metaf.geturl().rfind('/'):]
        else:
            self.info['size'] = 0
            self.info['fname'] = self.info['url']
        t = 0
        while os.path.exists(self.info['path'] + self.info['fname']):
            # Rename if file name already exists
            self.info['fname'] = self.info['fname'] + str('.{0}'.format(t))
            t += 1
        self.updateSize()
    
    def updateSize(self):
        #Size
        try:
            s = os.stat(self.info['path'] + self.info['fname'])
        except OSError:
            self.info['downloaded'] = 0
        else:
            self.info['downloaded'] = int(s.st_size)
        #Calculate progress percent
        try:
            p = self.info['downloaded'] * 100 / self.info['size']
        except ZeroDivisionError:
            p = 0
        self.info['progress'] = p
        
        self.info['remaining'] = self.info['size'] - self.info['downloaded']
        self.datasig.emit(self.id)

class ControlCore(QObject):
    #Control class which handles all downloads
    finishedsig = Signal(int)
    queuesig = Signal(int)
    def __init__(self, proxy=None, ddir=None, qmgr=2):
        super(ControlCore, self).__init__()
        #Initialize
        if ddir:
            self.ddir = ddir
        else:
            self.ddir = QDir().absolutePath() #default download location
        self.qmgr = QueueManager(int(qmgr))
        self.grabbers = []
        #Working directory and grabbers file
        #self.grabbers list contain pack of Grabber() and Metadata()
        if sys.platform == 'win32':
            location = str(os.environ['appdata'] + '/Firesoft')
        else:
            location = '.config/Firesoft'
        self.wd = QDir(location)
        if not self.wd.exists():
            #Create dir if not exists
            self.wd.mkpath(location)
        self.gfile = QFile(self.wd.absolutePath() + '/grabbers.dit')
        self.proxy = proxy
        if self.gfile.exists():
            self.gfile.open(QIODevice.ReadOnly)
            inp = QDataStream(self.gfile)
            #Check file format
            magno = inp.readInt32()
            if magno == 49:
                noel = inp.readInt32()
                for i in range(noel):
                    self.grabbers.append([])
                    self.grabbers[i].append(None)
                    self.grabbers[i].append(Metadata())
                    self.grabbers[i][1].info = inp.readQVariant() #Read/write info dict
                    self.grabbers[i][1].id = i
                    self.grabbers[i][0] = Grabber(self.grabbers[i][1].info['url'], self.grabbers[i][1].info['downloaded'], proxy=self.proxy)
                    self.signalConnector(self.grabbers[i][0], self.grabbers[i][1])
            self.gfile.close()
    
    def saveQueue(self):
        #Save list to file
        noel = len(self.grabbers)
        self.gfile.open(QIODevice.WriteOnly)
        out = QDataStream(self.gfile)
        #Write header with magic no
        out.writeInt32(49)
        #Write number of elements and queue list
        out.writeInt32(noel)
        for i in range(noel):
            out.writeQVariant(self.grabbers[i][1].info)
        self.gfile.close()
    
    def addGrabber(self, url, path):
        #Add new grabber and meta
        pack = []
        g = Grabber(url, 0, proxy=self.proxy)
        m = Metadata(url=url, id=len(self.grabbers), proxy=self.proxy)
        m.info['path'] = path
        m.start()
        pack.append(g)
        pack.append(m)
        self.grabbers.append(pack)
        self.signalConnector(g, m)
        self.updateMetas()
    
    def signalConnector(self, grabber, meta):
        #Create connections between grabbers and metas
        grabber.progress.speedsig.connect(meta.speedSig)
        grabber.progress.readsig.connect(meta.readSig)
        grabber.progress.ttfsig.connect(meta.ttfSig)
    
    def stopGrabber(self, id):
        #pause selected grabber
        if self.grabbers[id][0].running:
            self.qmgr.release()
        self.grabbers[id][0].stopJob()
        self.grabbers[id][1].info['speed'] = 0
        self.grabbers[id][1].info['ttf'] = 0
        self.grabbers[id][1].info['delta'] = 0
        self.grabbers[id][1].waiting = False
    
    def quitJob(self, id):
        if self.grabbers[id][0].running:
            self.grabbers[id][0].stopJob()
        self.grabbers[id][0].quit()
            
    def queueUpdated(self):
        for item in self.grabbers:
            if item[1].waiting is True:
                self.resumeGrabber(item[1].id)
                self.queuesig.emit(item[1].id)
    
    def finishedGrabber(self, id):
        self.qmgr.release()
        self.queueUpdated()
        self.finishedsig.emit(id)
    
    def resumeGrabber(self, id):
        #resume selected grabber
        if self.grabbers[id][0].running: #Check if already running
            return 0
        self.grabbers[id][0].fname = self.grabbers[id][1].info['fname']
        self.grabbers[id][0].savelocation = self.grabbers[id][1].info['path']
        self.grabbers[id][1].updateSize()
        if not self.grabbers[id][1].info['downloaded'] == self.grabbers[id][1].info['size'] or self.grabbers[id][1].info['size'] == 0:
            res = self.qmgr.request()
            if res is True:
                self.grabbers[id][0].runGrabber('resume', self.grabbers[id][1].info['throttle'])
                self.grabbers[id][1].waiting = False
            else:
                self.grabbers[id][1].waiting = True
        else:
            self.finishedGrabber(id)
    
    def removeGrabber(self, id):
        #remove selected grabber
        self.stopGrabber(id)
        self.grabbers[id][0].quit()
        del self.grabbers[id][1]
        del self.grabbers[id][0]
        del self.grabbers[id]
        self.updateMetas()
    
    def changePath(self, i, origpath, newpath):
        #Change path of job
        self.grabbers[i][1].info['path'] = newpath
        if os.path.exists(origpath + self.grabbers[i][1].info['fname']):
            #Move file if it exists
            #Check if destination file exists
            t = 0
            origname = self.grabbers[i][1].info['fname']
            while os.path.exists(newpath + self.grabbers[i][1].info['fname']):
                #Rename if file name already exists
                self.grabbers[i][1].info['fname'] = origname + str('.{0}'.format(t))
                t += 1
            shutil.move(origpath + self.grabbers[i][1].info['fname'], newpath + self.grabbers[i][1].info['fname'])
    
    def moveItem(self, f, t):
        #Move item from f to t
        p = self.grabbers.pop(f)
        self.grabbers.insert(t, p)
        self.updateMetas()
        
    def setSpeed(self, i, s, sb):
        #Set i's speed to s, speed button to sb
        self.grabbers[i][1].info['throttle'] = s
        self.grabbers[i][1].info['throttleb'] = sb
        if self.grabbers[i][0].running:
            self.grabbers[i][0].grabber.fo.opts.throttle = s
    
    def updateMetas(self):
        #Update id info on Meta objects
        for i in range(len(self.grabbers)):
            self.grabbers[i][1].id = i
    
    def setProxy(self, proxy):
        self.proxy = proxy
        for i in range(len(self.grabbers)):
            self.grabbers[i][0].proxy = proxy
            self.grabbers[i][1].info['proxy'] = proxy
