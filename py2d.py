from glob import glob
from py2deb import Py2deb
from lib.classes import AppInfo

info = AppInfo()

p = Py2deb(info.sysname)
p.author = info.devname
p.mail = info.devemail
p.description = info.description
p.url = info.url
p.depends = 'python, python-pyside.qtcore, python-pyside.qtgui'
p.license = 'gpl'
p.section = 'web'
p.arch = 'all'
p.postinstall = 'postinstall.sh'
p.postremove = 'postremove.sh'

p['/usr/share/applications'] = ['downloader.desktop']
p['/usr/lib/downloader'] = ['copyright', 'downloadit.pyw'] + glob('lib/*.py') + glob('lib/urlgrabber2/*.py') + glob('ubulib/*.py')
p['/usr/share/icons'] = ['res/icons/download.png|download.png']

p.generate(info.appversion, src=True)

