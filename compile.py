import shutil
import os
import sys
import platform
from cx_Freeze import setup, Executable

plat = None

includefiles = ['winlib/taskbar_32.tlb', 'winlib/taskbar_64.tlb']
excludes = ["Tkconstants", 'Tkinter', 'tcl']
includes = ["comtypes"]

if sys.platform == "win32":
    plat = "Win32GUI"
    iconfile = "res\icons\download.ico"
else:
    iconfile = "res/icons/download.png"

exe = Executable(
    script="downloadit.pyw",
    base=plat,
    icon=iconfile,
    )

setup(
    name = "Downloader",
    version = "0.8.16",
    description = "Download manager with minimalistic user interface",
    options = {"build_exe": {"includes":includes, "excludes": excludes, 'include_files':includefiles}},
    executables = [exe]
    )

if sys.platform == "win32" and platform.architecture()[0] == '64bit':
    shutil.copy(os.getcwd() + '/fix/QtCore4.dll', os.getcwd() + '/build/exe.win-amd64-2.7/')
    shutil.copy(os.getcwd() + '/fix/QtGui4.dll', os.getcwd() + '/build/exe.win-amd64-2.7/')
