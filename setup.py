from distutils.core import setup
from lib.classes import AppInfo

info = AppInfo()

setup(name = info.sysname,
    version = info.appversion,
    description = info.description,
    packages = ['lib', 'ubulib', 'lib.urlgrabber2'],
    scripts = ['downloadit.pyw'],
    author = info.devname,
    author_email = info.devemail,
    url = info.url,
    )
