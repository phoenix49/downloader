#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       win.py
#       
#       Copyright 2012 Said Babayev <phoenix49@gmail.com>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       
#       

import os
import platform
from PySide.QtCore import *
from ctypes import pythonapi, c_void_p, py_object

TBPF_NOPROGRESS = 0
TBPF_INDETERMINATE = 0x1
TBPF_NORMAL = 0x2
TBPF_ERROR = 0x4
TBPF_PAUSED = 0x8

class WinTaskbar(object):
    def __init__(self, winid, parent=None):
        super(WinTaskbar, self).__init__()
        pythonapi.PyCObject_AsVoidPtr.restype = c_void_p
        pythonapi.PyCObject_AsVoidPtr.argtypes = [ py_object ]
        self.winid = pythonapi.PyCObject_AsVoidPtr(winid)
        curdir = os.getcwd()
        if platform.architecture()[0] == '32bit':
            taskbarlib = curdir + '/winlib/taskbar_32.tlb'
        elif platform.architecture()[0] == '64bit':
            taskbarlib = curdir + '/winlib/taskbar_64.tlb'
        import comtypes.client as cc
        cc.GetModule(taskbarlib)
        import comtypes.gen.TaskbarLib as tbl
        self.taskbar = cc.CreateObject("{56FDF344-FD6D-11d0-958A-006097C9A090}", interface=tbl.ITaskbarList3)
        self.taskbar.HrInit()
        self.taskbar.SetProgressState(self.winid, TBPF_NORMAL)
        
    def updateView(self, progress, count):
        self.taskbar.SetProgressValue(self.winid,progress,100)
        if progress > 0:
            self.taskbar.SetProgressState(self.winid, TBPF_NORMAL)
        else:
            self.taskbar.SetProgressState(self.winid, TBPF_NOPROGRESS)

